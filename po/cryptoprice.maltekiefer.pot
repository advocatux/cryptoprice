# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the cryptoprice.maltekiefer package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: cryptoprice.maltekiefer\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-30 09:37+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Settings.qml:13
msgid "Settings"
msgstr ""

#: ../qml/Settings.qml:31
msgid "Automatic Updates every 5 seconds?"
msgstr ""

#: ../qml/Settings.qml:45
msgid "Your currency?"
msgstr ""

#: ../qml/Settings.qml:71
msgid "Source of prices?"
msgstr ""

#: CryptoPrice.desktop.in.h:1
msgid "CryptoPrice"
msgstr ""
