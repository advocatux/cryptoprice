import QtQuick 2.4
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
    id: aboutPage
    property int showDebug

    header: PageHeader {
    title: i18n.tr("Settings")
    flickable: flickable
    }

    Rectangle {
        height: units.gu(24)
    }
    Flickable {
        anchors.fill: parent
        contentHeight: configuration.childrenRect.height

        Column {
            id: configuration
            anchors.fill: parent

            ListItem.SingleValue {
            }
            ListItem.Standard {
                text: i18n.tr("Automatic Updates every 5 seconds?")
                enabled: true
                control: Switch {
                    id: enableAutomaticUpdates
                    checked: settings.automaticRefresh
                    onClicked: {
                        if(settings.automaticRefresh)
                            settings.automaticRefresh = false
                        else
                            settings.automaticRefresh = true
                    }
                }
            }
            ListItem.Standard {
                text: i18n.tr("Your currency?")
                enabled: true
                control: ComboBox {
                            id: currency
                            height: units.gu(5)
                            width: units.gu(20)
                            currentIndex: settings.currentIndex
                            textRole: "text"
                            model: ListModel {
                                id: currencyModel
                                ListElement { text: "CHF";  symbol: "SFr."; position: "right" }
                                ListElement { text: "EUR";  symbol: "€"; position: "right" }
                                ListElement { text: "GBP";  symbol: "£"; position: "left" }
                                ListElement { text: "USD";  symbol: "$"; position: "left" }
                            }
                            onCurrentIndexChanged: {
                                settings.currentIndex = currency.currentIndex
                                settings.userCurrency = currencyModel.get(currentIndex).text
                                settings.userCurrencySymbol = currencyModel.get(currentIndex).symbol
                                settings.userCurrencySymbolPosition = currencyModel.get(currentIndex).position
                                getPrice()
                            }
                            
                        }
            }
            ListItem.Standard {
                text: i18n.tr("Source of prices?")
                enabled: true
                control: ComboBox {
                            id: api
                            height: units.gu(5)
                            width: units.gu(20)
                            currentIndex: settings.apiIndex
                            textRole: "text"
                            model: ListModel {
                                id: apiModel
                                ListElement { text: "Coinbase";  code: "COINBASE" }
                                ListElement { text: "CEX.io";  code: "CEXIO" }
                            }
                            onCurrentIndexChanged: {
                                settings.apiIndex = api.currentIndex
                                settings.api = apiModel.get(currentIndex).code
                                getPrice()
                            }
                            
                        }
            }
        }   
    }

}