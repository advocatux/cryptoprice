/*
    ICONS: https://github.com/atomiclabs/cryptocurrency-icons/
*/

import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'cryptoprice.maltekiefer'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Settings {
        id: settings
        property bool automaticRefresh: false
        property int currentIndex: 3
        property int apiIndex: 0
        property string api: "COINBASE"
        property string userCurrency: "USD"
        property string userCurrencySymbol: "$"
        property string userCurrencySymbolPosition: "left"
    }

    Settings {
        id: curSettings
        property string btc: "0"
        property string eth: "0"
        property string xrp: "0"
        property string bch: "0"
        property string ltc: "0"
    }

    Timer {
        id: timer
        interval: 5000
        repeat: settings.automaticRefresh
        running: settings.automaticRefresh

        onTriggered:
        {
            getPrice()
        }
    }

    PageStack {
        id: pageStack
        Component.onCompleted: pageStack.push(mainPage)
 
        Page {
            id: mainPage
            anchors.fill: parent

            header: PageHeader {
                id: header
                title: 'CryptoPrice'
                trailingActionBar {
                    actions: [
                        Action {
                            iconName: "settings"
                            text: "settings"

                            onTriggered: pageStack.push(Qt.resolvedUrl("Settings.qml"))
                        },
                        Action {
                            iconName: "reload"
                            text: "refresh"

                            onTriggered: getPrice()
                        }
                    ]
                }
            }
            Flickable {
                anchors.fill: parent
                anchors.top: parent.bottom
                anchors.topMargin: units.gu(6.3)
                contentHeight: main.height + units.gu(10) + main.anchors.topMargin
                id: flick

                Column {
                    id: main
                    anchors.fill: parent
                    RowLayout {
                        spacing: -units.gu(5)
                        anchors.topMargin: units.gu(15)
                        width: parent.width

                        Item {
                            Layout.preferredWidth: (parent.width / 2 )
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            height: units.gu(4)
                            Image {
                                source: Qt.resolvedUrl("assets/btc.svg")
                                width: units.gu(4)
                                height: units.gu(4)
                                fillMode: Image.PreserveAspectFit
                            }
                        }

                        Label {
                            Layout.preferredWidth: (parent.width / 2 )
                            id: lb_bitcoin
                            text: "N/A"
                            horizontalAlignment: Text.AlignRight
                            font.weight: Font.DemiBold
                            wrapMode: Text.WordWrap
                            textSize: Label.XLarge
                        }

                    }
                    ListItem.ThinDivider {}
                    RowLayout {
                        spacing: -units.gu(5)
                        anchors.topMargin: units.gu(15)
                        width: parent.width

                        Item {
                            Layout.preferredWidth: (parent.width / 2 )
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            height: units.gu(4)
                            Image {
                                source: Qt.resolvedUrl("assets/eth.svg")
                                width: units.gu(4)
                                height: units.gu(4)
                                fillMode: Image.PreserveAspectFit
                            }
                        }

                        Label {
                            Layout.preferredWidth: (parent.width / 2 )
                            id: lb_eth
                            text: "N/A"
                            horizontalAlignment: Text.AlignRight
                            font.weight: Font.DemiBold
                            wrapMode: Text.WordWrap
                            textSize: Label.XLarge

                        }

                    }
                  ListItem.ThinDivider {}
                    RowLayout {
                        spacing: -units.gu(5)
                        anchors.topMargin: units.gu(15)
                        width: parent.width

                        Item {
                            Layout.preferredWidth: (parent.width / 2 )
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            height: units.gu(4)
                            Image {
                                source: Qt.resolvedUrl("assets/xrp.svg")
                                width: units.gu(4)
                                height: units.gu(4)
                                fillMode: Image.PreserveAspectFit
                            }
                        }

                        Label {
                            Layout.preferredWidth: (parent.width / 2 )
                            id: lb_xrp
                            text: "N/A"
                            horizontalAlignment: Text.AlignRight
                            font.weight: Font.DemiBold
                            wrapMode: Text.WordWrap
                            textSize: Label.XLarge

                        }

                    }
                    ListItem.ThinDivider {}
                    RowLayout {
                        spacing: -units.gu(5)
                        anchors.topMargin: units.gu(15)
                        width: parent.width

                        Item {
                            Layout.preferredWidth: (parent.width / 2 )
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            height: units.gu(4)
                            Image {
                                source: Qt.resolvedUrl("assets/bch.svg")
                                width: units.gu(4)
                                height: units.gu(4)
                            }
                        }

                        Label {
                            Layout.preferredWidth: (parent.width / 2 )
                            id: lb_bch
                            text: "N/A"
                            horizontalAlignment: Text.AlignRight
                            font.weight: Font.DemiBold
                            wrapMode: Text.WordWrap
                            textSize: Label.XLarge

                        }

                    }
                    ListItem.ThinDivider {}
                    RowLayout {
                        spacing: -units.gu(5)
                        anchors.topMargin: units.gu(15)
                        width: parent.width

                        Item {
                            Layout.preferredWidth: (parent.width / 2 )
                            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
                            height: units.gu(4)
                            Image {
                                source: Qt.resolvedUrl("assets/ltc.svg")
                                width: units.gu(4)
                                height: units.gu(4)
                            }
                        }

                        Label {
                            Layout.preferredWidth: (parent.width / 2 )
                            id: lb_ltc
                            text: "N/A"
                            horizontalAlignment: Text.AlignRight
                            font.weight: Font.DemiBold
                            wrapMode: Text.WordWrap
                            textSize: Label.XLarge

                        }

                    }
                    ListItem.ThinDivider {}
                }
            }

            Component.onCompleted: {
                getPrice()
            }
        }
    }           

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('./'));

            importModule('cryptoprice', function() {
                console.log("-------------- python loaded");
            });
        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }

    function getPrice() {
        python.call('cryptoprice.get_latest_price', [settings.api, 'BTC', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_bitcoin.text = settings.userCurrencySymbol + " " + (returnValue).toFixed(2)
            else
                lb_bitcoin.text = (returnValue).toFixed(2) + " " + settings.userCurrencySymbol

            if (parseFloat(curSettings.btc) > returnValue)
                lb_bitcoin.color = "#e60000"

            if (parseFloat(curSettings.btc) < returnValue)
                lb_bitcoin.color = "#2eb82e"

            curSettings.btc = returnValue

        })
        python.call('cryptoprice.get_latest_price', [settings.api, 'ETH', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_eth.text = settings.userCurrencySymbol + " " + (returnValue).toFixed(2)
            else
                lb_eth.text = (returnValue).toFixed(2) + " " + settings.userCurrencySymbol

            if (parseFloat(curSettings.eth) > returnValue)
                lb_eth.color = "#e60000"

            if (parseFloat(curSettings.eth) < returnValue)
                lb_eth.color = "#2eb82e"

            curSettings.eth = returnValue
        })
        python.call('cryptoprice.get_latest_price', [settings.api, 'XRP', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_xrp.text = settings.userCurrencySymbol + " " + (returnValue).toFixed(2)
            else
                lb_xrp.text = (returnValue).toFixed(2) + " " + settings.userCurrencySymbol

            if (parseFloat(curSettings.eth) > returnValue)
                lb_xrp.color = "#e60000"

            if (parseFloat(curSettings.eth) < returnValue)
                lb_xrp.color = "#2eb82e"

            curSettings.xrp = returnValue
        })
        python.call('cryptoprice.get_latest_price', [settings.api, 'BCH', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_bch.text = settings.userCurrencySymbol + " " + (returnValue).toFixed(2)
            else
                lb_bch.text = (returnValue).toFixed(2) + " " + settings.userCurrencySymbol

            if (parseFloat(curSettings.eth) > returnValue)
                lb_bch.color = "#e60000"

            if (parseFloat(curSettings.eth) < returnValue)
                lb_bch.color = "#2eb82e"

            curSettings.bch = returnValue
        })
        python.call('cryptoprice.get_latest_price', [settings.api, 'LTC', settings.userCurrency], function(returnValue) {
            if (settings.userCurrencySymbolPosition == "left")
                lb_ltc.text = settings.userCurrencySymbol + " " + (returnValue).toFixed(2)
            else
                lb_ltc.text = (returnValue).toFixed(2) + " " + settings.userCurrencySymbol

            if (parseFloat(curSettings.eth) > returnValue)
                lb_ltc.color = "#e60000"

            if (parseFloat(curSettings.eth) < returnValue)
                lb_ltc.color = "#2eb82e"

            curSettings.ltc = returnValue
        })

        done()
    }

    function done() {
        console.log("-------------- updated all cryptoprices")
    }
}
