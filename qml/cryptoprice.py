import requests

def get_latest_price(api, currencie, real):

    COINBASE_API_URL = "https://api.coinbase.com/v2/prices/%s-%s/spot" % (currencie, real)
    CEXIO_API_URL = "https://cex.io/api/last_price/%s/%s" % (currencie.upper(), real.upper())

    if api == "COINBASE":
        response = requests.get(COINBASE_API_URL)
        response_json = response.json()
        return float(response_json['data']['amount'])
    else:
        response = requests.get(CEXIO_API_URL)
        response_json = response.json()
        return float(response_json['lprice']) 
